const electron = require('electron');

const { app, BrowserWindow } = electron;

let window, backend_process;

function createBackend() {
    backend_process = require('child_process').spawn(`${__dirname}/dist/backend/backend`);
}

function createFrontend() {
    const { width, height } = electron.screen.getPrimaryDisplay().size

    window = new BrowserWindow({
        minWidth: width * 0.6,
        minHeight: height * 0.6,
        width: width * 0.9,
        height: height,
        backgroundColor: '#ffffff',
        icon: `file://${__dirname}/logo.png`
    });

    window.loadURL(`file://${__dirname}/index.html`);

    window.on('closed', function () {
        window = null;
        backend_process.kill();
    });
}

app.on('ready', () => {
    createBackend();
    createFrontend();
});

app.on('window-all-closed', function () {
    if (process.platform !== 'darwin') {
        app.quit();
        backend_process.kill();
    }
});

app.on('activate', function () {
    if (window === null) {
        createBackend();
        createFrontend();
    }
});